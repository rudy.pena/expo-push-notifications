# expo-push-notifications

## Getting started

To start the application, you will need to install the dependencies and then just start the application.

```
npm i

npm run start
```

## Development

When you have the application runnings, there are a few ways to view the application such as the iOS Simulator, an Android Simulator, and/or Expo's Go application (which allows you to view it on your phone at real time).

- [Expo Go](https://expo.dev/client) - More on how it works can be found [here](https://docs.expo.dev/workflow/expo-go/).
- [iOS Simulator](https://docs.expo.dev/workflow/ios-simulator/)
- [Android Simulator](https://docs.expo.dev/workflow/android-studio-emulator/) -- [Android Emulator](https://developer.android.com/studio)

## Tools

- `expo-image-picker` - Docs [here](https://docs.expo.dev/versions/latest/sdk/imagepicker/).
- `expo-location` - Docs [here](https://docs.expo.dev/versions/v46.0.0/sdk/location/).
- `react-native-maps` - Docs [here](https://docs.expo.dev/versions/v46.0.0/sdk/map-view/).
