import { useEffect } from "react";

import { StatusBar } from "expo-status-bar";

import { Alert, Button, Platform, StyleSheet, Text, View } from "react-native";

import * as Notifications from "expo-notifications";

async function requestPermissionsAsync() {
  return await Notifications.requestPermissionsAsync({
    ios: {
      allowAlert: true,
      allowBadge: true,
      allowSound: true,
      allowAnnouncements: true,
    },
  });
}

Notifications.setNotificationHandler({
  handleNotification: async () => {
    return {
      shouldPlaySound: true,
      shouldSetBadge: true,
      shouldShowAlert: true,
    };
  },
});

export default function App() {
  // Documentation: https://docs.expo.dev/push-notifications/sending-notifications/
  // Expo's Push Notification Tool for Sending Notifications: https://expo.dev/notifications
  useEffect(() => {
    const configurePushNotifications = async () => {
      const { status } = await Notifications.getPermissionsAsync();
      let finalStatus = status;

      if (finalStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }

      if (finalStatus !== "granted") {
        Alert.alert(
          "Permissions required",
          "Push notifications need the approriate permissions."
        );
        return;
      }

      // This will not work on emulators. We need to test on a real device such as using ExpoGo.
      const pushTokenData = await Notifications.getExpoPushTokenAsync();
      console.log(pushTokenData);

      if (Platform.OS === "android") {
        Notifications.setNotificationChannelAsync("default", {
          name: "default",
          importance: Notifications.AndroidImportance.DEFAULT,
        });
      }
    };

    configurePushNotifications();
  }, []);

  // Ask for Notification Permissions right after the bat
  useEffect(() => {
    requestPermissionsAsync();
  }, []);

  useEffect(() => {
    const subscription = Notifications.addNotificationReceivedListener(
      (notification) => {
        console.log("NOTIFICATION RECEIVED");
        console.log(notification);
        const userName = notification.request.content.data.userName;
        console.log("username:", userName);
      }
    );

    const subcription2 = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        console.log("NOTIFICATION RESPONSE RECEIVED");
        console.log(response);
        const userName = response.request.content.data.userName;
        console.log("username:", userName);
      }
    );

    return () => {
      subscription.remove();
      subcription2.remove();
    };
  }, []);

  const scheduleNotificationHandler = () => {
    Notifications.scheduleNotificationAsync({
      content: {
        title: "My first local notification.",
        body: "This is the body of the notification.",
        data: {
          userName: "Max",
        },
      },
      trigger: {
        seconds: 1,
      },
    });
  };

  const sendPushNotifications = () => {
    fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        to: `INSERT_EXPONENT_PUSH_TOKEN[VALUE_HERE]`,
        title: "Test - send from a device!",
        body: "This is a test!",
      }),
    });
  };

  return (
    <View style={styles.container}>
      <Text>Hello World!!!!</Text>
      <Button
        title="Schedule Notification"
        onPress={scheduleNotificationHandler}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
